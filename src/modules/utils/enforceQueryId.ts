import { get, intersection, set } from 'lodash';
import { ObjectId } from 'mongodb';

const INVALID_MONGO_ID = '000000000000000000000000';
const IN = '$in';

export const enforceQueryId = (where: any, field: string, listId: any[]) => {
  let queryId = get(where, field);

  if (!queryId) {
    set(where, field, { $in: listId });
    return where;
  }

  const listIdStr = listId.map(id => id.toString());
  if (Array.isArray(queryId[IN])) {
    queryId = queryId[IN].map((item: { toString: () => string }) =>
      item.toString(),
    );
    const commonId = intersection(queryId, listIdStr);
    set(where, field, { $in: commonId.map(id => new ObjectId(id)) });
    return where;
  }

  if (typeof queryId === 'object' && queryId.constructor.name === 'ObjectID') {
    queryId = queryId.toString();
  }

  if (listIdStr.indexOf(queryId) < 0) {
    set(where, field, new ObjectId(INVALID_MONGO_ID));
  }
  return where;
};
