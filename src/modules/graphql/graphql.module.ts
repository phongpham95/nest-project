import { buildSchema } from 'type-graphql';
import { Container } from 'typedi';

import { IContext } from '@graphql/types/graphql-context';
import { DbLoaderManager } from '@modules/base/dataloader';
import { authChecker, getCurrentUserInfo, IIncomingMessage } from '@modules/graphql/auth';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

interface IContextInput {
  req: IIncomingMessage;
}

@Module({
  imports: [
    GraphQLModule.forRootAsync({
      useFactory: async () => {
        const schema = await buildSchema({
          authChecker,
          resolvers: [() => null],
          container: Container,
        });
        const config = {
          schema,
          context: async ({ req }: IContextInput): Promise<IContext> => ({
            loaderManager: new DbLoaderManager(),
            authInfo: await getCurrentUserInfo(req),
          }),
        };
        return config;
      },
    }),
  ],
})
export class GraphQlModule {}
