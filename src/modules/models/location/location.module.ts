import { TypegooseModule } from 'nestjs-typegoose';

import { Module } from '@nestjs/common';

import { Location } from './location.model';
import { LocationResolver } from './location.resolver';
import { LocationService } from './location.service';

@Module({
  imports: [TypegooseModule.forFeature([Location])],
  providers: [LocationService, LocationResolver],
  exports: [LocationService],
})
export class LocationModule {}
