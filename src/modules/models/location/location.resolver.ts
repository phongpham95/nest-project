import { Ctx, FieldResolver, Resolver, Root } from 'type-graphql';
import { Container } from 'typedi';

import { IContext } from '@graphql/types/graphql-context';
import { User } from '@models/user/user.model';
import { ReturnModelType } from '@typegoose/typegoose';

import { BaseLocationResolver } from './location.base';
import { Location } from './location.model';
import { LocationService } from './location.service';

@Resolver(Location)
export class LocationResolver extends BaseLocationResolver {
  constructor(private readonly service: LocationService) {
    super();
    Container.set(LocationResolver, this);
  }
  @FieldResolver(() => User, { nullable: true })
  public async creator(
    @Root() user: ReturnModelType<typeof User>,
    @Ctx() { loaderManager }: IContext,
  ): Promise<User> {
    if (!user.creatorId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(user.creatorId.toString());
  }

  @FieldResolver(() => User, { nullable: true })
  public async customer(
    @Root() user: ReturnModelType<typeof User>,
    @Ctx() { loaderManager }: IContext,
  ): Promise<User> {
    if (!user.customerId) {
      return;
    }
    const loader = loaderManager.getLoader(User);
    return loader.load(user.customerId.toString());
  }
}
