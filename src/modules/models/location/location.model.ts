import { ObjectId } from 'mongodb';
import { ID, InputType, ObjectType } from 'type-graphql';

import { BaseModel } from '@base/base.model';
import { ancestorPlugin, modelAccessPlugin, ownerPlugin } from '@database/plug-in';
import { Field } from '@modules/graphql/graphql-decorators';
import { modelOptions, plugin, prop } from '@typegoose/typegoose';

@InputType('LocationInput')
@ObjectType()
@plugin(ownerPlugin)
@plugin(modelAccessPlugin)
@plugin(ancestorPlugin)
@modelOptions({ schemaOptions: { collection: 'Location' } })
export class Location extends BaseModel {
  @Field({ filter: true, sort: true })
  @prop({ required: true, unique: true })
  public name: string;

  @Field()
  @prop()
  public address: string;

  @Field({ filter: true, sort: true })
  @prop({ required: true })
  public categoryId: string;

  @Field(() => ID, { nullable: true, filter: true, sort: true })
  @prop()
  public creatorId?: ObjectId;

  @Field(() => ID, { nullable: true, filter: true, sort: true })
  @prop()
  public customerId?: ObjectId;
}
