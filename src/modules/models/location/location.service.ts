import { InjectModel } from 'nestjs-typegoose';

import { BaseService } from '@base/base.service';
import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';

import { Location } from './location.model';

@Injectable()
export class LocationService extends BaseService<Location> {
  constructor(
    @InjectModel(Location)
    protected readonly model: ReturnModelType<typeof Location>,
  ) {
    super();
  }
}
