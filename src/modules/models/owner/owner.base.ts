import { createBaseResolver } from '@base/base.resolver';
import {
    PERMISSION_READ_ALL, PERMISSION_READ_ALL_OWNERS, PERMISSION_READ_OWNER
} from '@base/decentralization';

import { Owner } from './owner.model';

const { BaseResolver: BaseOwnerResolver } = createBaseResolver<Owner>(Owner, {
  action: {
    read: {
      role: [
        PERMISSION_READ_OWNER,
        PERMISSION_READ_ALL_OWNERS,
        PERMISSION_READ_ALL,
      ],
    },
    create: { enable: false },
    update: { enable: false },
    remove: { enable: false },
  },
});

export { BaseOwnerResolver };
