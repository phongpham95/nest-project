import { InputType, ObjectType } from 'type-graphql';

import { BaseModel } from '@base/base.model';
import { Field } from '@modules/graphql/graphql-decorators';
import { modelOptions, prop } from '@typegoose/typegoose';

@InputType('OwnerInput')
@ObjectType()
@modelOptions({ schemaOptions: { collection: 'Owner' } })
export class Owner extends BaseModel {
  @Field({ filter: true, sort: true })
  @prop()
  public userId: string;

  @Field({ filter: true, sort: true })
  @prop()
  public resourceId: string;

  @Field({ filter: true })
  @prop()
  public model: string;
}
