import { Resolver } from 'type-graphql';
import { Container } from 'typedi';

import { BaseOwnerResolver } from './owner.base';
import { Owner } from './owner.model';
import { OwnerService } from './owner.service';

@Resolver(Owner)
export class RoleResolver extends BaseOwnerResolver {
  constructor(private readonly service: OwnerService) {
    super();
    Container.set(RoleResolver, this);
  }
}
