import { Arg, Authorized, ObjectType, Resolver } from 'type-graphql';

import { PERMISSION_AUTHENTICATED } from '@base/decentralization';
import { StandardMutationError } from '@base/error';
import { Field, Mutation, Query } from '@graphql/graphql-decorators';
import { File } from '@modules/models/file/file.model';

import { FileService } from './file.service';

@ObjectType()
export class FilePayload {
  @Field(() => File, { nullable: true })
  public data?: File;

  @Field({ nullable: true })
  public error?: StandardMutationError;
}

@Resolver(File)
export class FileResolver {
  constructor(private readonly service: FileService) {}

  @Authorized(PERMISSION_AUTHENTICATED)
  @Mutation(() => FilePayload)
  public async createUploadLink(): Promise<FilePayload> {
    return this.service.createUploadLink();
  }

  @Authorized(PERMISSION_AUTHENTICATED)
  @Query(() => FilePayload)
  public async viewUploadLink(@Arg('id') id: string): Promise<FilePayload> {
    return this.service.viewUploadLink(id);
  }
}
