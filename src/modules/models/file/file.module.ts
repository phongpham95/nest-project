import { FileResolver } from '@modules/models/file/file.resolver';
import { Module } from '@nestjs/common';

import { FileService } from './file.service';

@Module({
  providers: [FileResolver, FileService],
  exports: [FileService],
})
export class FileModule {}
