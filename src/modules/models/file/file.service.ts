import { invalidMutation } from '@base/error';
import { minioClient } from '@modules/utils/minio';

export const uploadFileDefaultExpire = 3600;
export const bucketName = 'client';

export class FileService {
  public async createUploadLink() {
    const idWithExt = `file-${Date.now()}`;
    let path = '';

    try {
      path = await minioClient.presignedPutObject(
        bucketName,
        idWithExt,
        uploadFileDefaultExpire,
      );
    } catch (err) {
      return invalidMutation;
    }
    const data = { path, id: idWithExt };

    return {
      data,
    };
  }

  public async viewUploadLink(id: string) {
    let path = '';

    try {
      path = await minioClient.presignedGetObject(bucketName, id);
    } catch (err) {
      return invalidMutation;
    }
    const data = { path, id };

    return {
      data,
    };
  }
}
