import { pickBy, some } from 'lodash';
import {
    Arg, Args, ArgsType, Authorized, ClassType, ID, InputType, Int, ObjectType, registerEnumType,
    Resolver
} from 'type-graphql';
import { TypeValueThunk } from 'type-graphql/dist/decorators/types';
import { FieldMetadata } from 'type-graphql/dist/metadata/definitions';
import { getMetadataStorage } from 'type-graphql/dist/metadata/getMetadataStorage';
import Container from 'typedi';

import { BaseModel } from '@base/base.model';
import { StandardMutationError } from '@base/error';
import {
    Field, FILTER_METADATA_KEY, Mutation, Query, SORT_METADATA_KEY
} from '@graphql/graphql-decorators';
import { SortDirection } from '@modules/graphql/types';

import { BaseService } from './base.service';
import { PERMISSION_AUTHENTICATED } from './decentralization';
import { OPERATORS_FIELDNAME, processFilters } from './utils';

export type Empty = null | undefined;
export type Status = number;

interface IOptions {
  role?: string[];
  enable?: boolean;
}

interface ICRUDOptions {
  read?: IOptions;
  create?: IOptions;
  update?: IOptions;
  remove?: IOptions;
}

interface IBaseResolverOptions {
  action?: ICRUDOptions;
}

registerEnumType(SortDirection, { name: 'SortDirection' });

function addArrayOperators(cls: ClassType, getType: TypeValueThunk) {
  Field(() => [getType()], { nullable: true })(cls.prototype, 'in');
  Field(() => [getType()], { nullable: true })(cls.prototype, 'nin');
}
function addRangeOperators(cls: ClassType, getType: TypeValueThunk) {
  Field(() => getType(), { nullable: true })(cls.prototype, 'gt');
  Field(() => getType(), { nullable: true })(cls.prototype, 'gte');
  Field(() => getType(), { nullable: true })(cls.prototype, 'lt');
  Field(() => getType(), { nullable: true })(cls.prototype, 'lte');
}

@InputType()
class StringOperatorArgs {}
addArrayOperators(StringOperatorArgs, () => String);

@InputType()
class NumberOperatorArgs {}
addArrayOperators(NumberOperatorArgs, () => Number);
addRangeOperators(NumberOperatorArgs, () => Number);

@InputType()
class DateOperatorArgs {}
addRangeOperators(DateOperatorArgs, () => Date);

export function createBaseResolver<TModel extends BaseModel>(
  ModelCls: ClassType<TModel>,
  options?: IBaseResolverOptions,
) {
  const modelName = ModelCls.name;
  const {
    action: { read, create, update, remove },
  } = options;

  @InputType(`${modelName}FilterOperator`)
  class WhereFilterOperatorArgs {}

  @InputType(`${modelName}Filter`)
  class WhereFilterArgs {
    @Field(() => [ID], { nullable: true })
    public _ids: [string];

    @Field(() => WhereFilterArgs, { nullable: true })
    public AND: [WhereFilterArgs];

    @Field(() => WhereFilterArgs, { nullable: true })
    public OR: [WhereFilterArgs];
  }

  @InputType(`${modelName}Sort`)
  class SortFilterArgs {}

  @InputType(`${modelName}UpdateArg`)
  class UpdateField {}

  @ArgsType()
  class UpdateArgs {
    @Field()
    public _id: string;

    @Field()
    public record: UpdateField;
  }

  const fields: FieldMetadata[] = (getMetadataStorage() as any).getClassFieldMetadata(
    ModelCls,
  );
  const baseFields: FieldMetadata[] = (getMetadataStorage() as any).getClassFieldMetadata(
    BaseModel,
  );

  const notIncluedField = ['creatorId', 'customerId', 'password'];

  [...fields].forEach(field => {
    if (!some(notIncluedField, item => item === field.name)) {
      const type = field.getType();
      if (field.typeOptions && field.typeOptions.array) {
        Field(() => [type], { nullable: true })(
          UpdateField.prototype,
          field.name,
        );
      } else {
        Field(() => type, { nullable: true })(
          UpdateField.prototype,
          field.name,
        );
      }
    }
  });

  let hasOperator = false;
  [...fields, ...baseFields].forEach(field => {
    const filterEnabled = Reflect.getMetadata(
      FILTER_METADATA_KEY,
      ModelCls.prototype,
      field.name,
    );
    const sortEnabled = Reflect.getMetadata(
      SORT_METADATA_KEY,
      ModelCls.prototype,
      field.name,
    );

    if (filterEnabled) {
      Field(field.getType, { nullable: true })(
        WhereFilterArgs.prototype,
        field.name,
      );
      if (field.getType() === String) {
        hasOperator = true;
        Field(() => StringOperatorArgs, { nullable: true })(
          WhereFilterOperatorArgs.prototype,
          field.name,
        );
      }
      if (field.getType() === Date) {
        hasOperator = true;
        Field(() => DateOperatorArgs, { nullable: true })(
          WhereFilterOperatorArgs.prototype,
          field.name,
        );
      }
      if (field.getType() === Number) {
        hasOperator = true;
        Field(() => NumberOperatorArgs, { nullable: true })(
          WhereFilterOperatorArgs.prototype,
          field.name,
        );
      }
    }
    if (sortEnabled) {
      Field(() => SortDirection, { nullable: true })(
        SortFilterArgs.prototype,
        field.name,
      );
    }
  });

  if (hasOperator) {
    Field(() => WhereFilterOperatorArgs, { nullable: true })(
      WhereFilterArgs.prototype,
      OPERATORS_FIELDNAME,
    );
  }

  @ArgsType()
  class FindOneFilterArgs {
    @Field(() => WhereFilterArgs, { nullable: true })
    public where: WhereFilterArgs;

    @Field(() => SortFilterArgs, { nullable: true })
    public sort: SortFilterArgs;
  }

  @ArgsType()
  class FindManyFilterArgs {
    @Field(() => Int, { nullable: true })
    public skip?: number = 0;

    @Field(() => Int, { nullable: true })
    public limit?: number;

    @Field(() => WhereFilterArgs, { nullable: true })
    public where?: WhereFilterArgs;

    @Field(() => SortFilterArgs, { nullable: true })
    public sort?: SortFilterArgs;
  }

  @ArgsType()
  class CountArgs {
    @Field(() => WhereFilterArgs, { nullable: true })
    public where?: WhereFilterArgs;
  }

  @ObjectType(`${modelName}BasePayload`)
  class BasePayload {
    @Field(() => ModelCls, { nullable: true })
    public data?: TModel;

    @Field({ nullable: true })
    public error?: StandardMutationError;
  }

  @Resolver({ isAbstract: true })
  class BaseResolver {
    protected readonly service: BaseService<TModel>;
    constructor() {
      Container.set(BaseResolver, this);
    }

    @Authorized(read.role || PERMISSION_AUTHENTICATED)
    @Query(() => ModelCls, {
      name: `find${modelName}ById`,
      nullable: true,
      enable: read.enable,
    })
    public async findById(@Arg('_id') _id: string): Promise<TModel> {
      return this.service.findById(_id);
    }

    @Authorized(read.role || PERMISSION_AUTHENTICATED)
    @Query(() => ModelCls, {
      nullable: true,
      name: `findOne${modelName}`,
      enable: read.enable,
    })
    public async findOne(@Args() args: FindOneFilterArgs): Promise<TModel> {
      const { where: filter, sort } = args;
      const where = processFilters(pickBy(filter, val => !!val));
      return this.service.findOne({ where, ...(sort && { sort }) });
    }

    @Authorized(read.role || PERMISSION_AUTHENTICATED)
    @Query(() => [ModelCls], {
      name: `findMany${modelName}`,
      enable: read.enable,
    })
    public async findMany(@Args() args: FindManyFilterArgs): Promise<TModel[]> {
      const { skip, limit, sort, where: filter } = args;

      const where = processFilters(pickBy(filter, val => !!val));
      return this.service.findMany({
        where,
        ...(sort && { sort }),
        ...(skip && { skip }),
        ...(limit && { limit }),
      });
    }

    @Authorized(read.role || PERMISSION_AUTHENTICATED)
    @Query(() => Int, {
      name: `count${modelName}`,
      nullable: true,
      enable: read.enable,
    })
    public async count(@Args() { where: filter }: CountArgs): Promise<number> {
      const where = processFilters(pickBy(filter, val => !!val));
      return this.service.count(where);
    }

    @Authorized(create.role || PERMISSION_AUTHENTICATED)
    @Mutation(() => BasePayload, {
      name: `create${modelName}`,
      enable: create.enable,
    })
    public async create(
      @Arg('record', () => ModelCls) record: TModel,
    ): Promise<BasePayload> {
      return this.service.create(record);
    }

    @Authorized(update.role || PERMISSION_AUTHENTICATED)
    @Mutation(() => BasePayload, {
      name: `update${modelName}ById`,
      enable: update.enable,
    })
    public async updateById(
      @Args() { _id, record }: UpdateArgs,
    ): Promise<BasePayload> {
      return this.service.updateById(_id, record as TModel);
    }

    @Authorized(remove.role || PERMISSION_AUTHENTICATED)
    @Mutation(() => BasePayload, {
      name: `remove${modelName}ById`,
      enable: remove.enable,
    })
    public async removeById(@Arg('_id') _id: string): Promise<BasePayload> {
      return this.service.removeById(_id);
    }
  }

  return {
    BaseResolver: BaseResolver as any,
    WhereFilterArgs: WhereFilterArgs as any,
    SortFilterArgs: SortFilterArgs as any,
    FindOneFilterArgs: FindOneFilterArgs as any,
    FindManyFilterArgs: FindManyFilterArgs as any,
  };
}
