import { ObjectId } from 'mongodb';

import { ClassType } from '@database/types';
import { IAuthInfo } from '@graphql/auth';
import { RequestContext } from '@modules/context';
import { Injectable } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';

import { invalidMutation } from './error';

@Injectable()
export class BaseService<TModel> {
  protected readonly model: ReturnModelType<ClassType<TModel>>;

  public async count(where?: any) {
    const authInfo: IAuthInfo = await RequestContext.authInfo();

    return this.model.find(where, null, { accessToken: authInfo }).count();
  }

  public async findById(_id: string) {
    const authInfo: IAuthInfo = await RequestContext.authInfo();

    return this.model.findOne({ _id: new ObjectId(_id) }, null, {
      accessToken: authInfo,
    });
  }

  public async findOne(filter?: any) {
    const authInfo: IAuthInfo = await RequestContext.authInfo();

    const { sort, where } = filter;
    return this.model.findOne(where, null, { sort, accessToken: authInfo });
  }

  public async findMany(filter?: any) {
    const authInfo: IAuthInfo = await RequestContext.authInfo();

    const { where, sort, skip, limit } = filter;
    return this.model.find(where, null, {
      skip,
      limit,
      sort,
      accessToken: authInfo,
    });
  }

  public async create(record: TModel) {
    const r = await this.model.create(record);
    if (!r) {
      return invalidMutation;
    }

    return { data: r };
  }

  public async updateById(_id: string, record: TModel) {
    const r = await this.findById(_id);
    if (!r) {
      return invalidMutation;
    }

    Object.assign(r, record);
    await r.save();

    return { data: r };
  }

  public async removeById(_id: string) {
    const r = await this.findById(_id);
    if (!r) {
      return invalidMutation;
    }

    await r.remove();
    Object.assign(r, { _id });

    return { data: r };
  }
}
