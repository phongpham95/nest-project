const allRoles = [];

export const PERMISSION_SUPERADMIN = 'superadmin';
allRoles.push({
  roleId: PERMISSION_SUPERADMIN,
  description: 'SUPERADMIN',
});

export const PERMISSION_AUTHENTICATED = '$authenticated';
allRoles.push({
  roleId: PERMISSION_AUTHENTICATED,
  description: '$authenticated',
});

export const PERMISSION_EVERYONE = '$everyone';
allRoles.push({
  roleId: PERMISSION_EVERYONE,
  description: '$everyone',
});

export const PERMISSION_UNAUTHENTICATED = '$unauthenticated';
allRoles.push({
  roleId: PERMISSION_UNAUTHENTICATED,
  description: '$unauthenticated',
});

export const PERMISSION_READ_ALL = 'all.read';
allRoles.push({
  roleId: PERMISSION_READ_ALL,
  description: 'Xem tất cả',
});

export const PERMISSION_READ_ALL_OF_CUSTOMER = 'allOfCustomer.read';
allRoles.push({
  roleId: PERMISSION_READ_ALL_OF_CUSTOMER,
  description: 'Xem tất cả Customer',
});

export const PERMISSION_CREATE_USER = 'user.create';
allRoles.push({
  roleId: PERMISSION_CREATE_USER,
  description: 'Tạo tài khoản',
});

export const PERMISSION_UPDATE_USER = 'user.update';
allRoles.push({
  roleId: PERMISSION_UPDATE_USER,
  description: 'Cập nhật tài khoản',
});

export const PERMISSION_DELETE_USER = 'user.delete';
allRoles.push({
  roleId: PERMISSION_DELETE_USER,
  description: 'Xóa tài khoản',
});

export const PERMISSION_READ_USER = 'user.read';
allRoles.push({
  roleId: PERMISSION_READ_USER,
  description: 'Xem tài khoản',
});

export const PERMISSION_READ_ALL_USERS = 'allUsers.read';
allRoles.push({
  roleId: PERMISSION_READ_ALL_USERS,
  description: 'Xem tất cả tài khoản',
});

export const PERMISSION_READ_ALL_USERS_OF_CUSTOMER = 'allUsersOfCustomer.read';
allRoles.push({
  roleId: PERMISSION_READ_ALL_USERS_OF_CUSTOMER,
  description: 'Xem tất cả tài khoản của Customer',
});

export const PERMISSION_READ_OWNER = 'owner.read';
allRoles.push({
  roleId: PERMISSION_READ_OWNER,
  description: 'Xem Owner',
});

export const PERMISSION_READ_ALL_OWNERS = 'allOwners.read';
allRoles.push({
  roleId: PERMISSION_READ_ALL_OWNERS,
  description: 'Xem tất cả Owner',
});

export const PERMISSION_READ_ROLE = 'role.read';
allRoles.push({
  roleId: PERMISSION_READ_ROLE,
  description: 'Xem Role',
});

export const PERMISSION_READ_ALL_ROLES = 'allRoles.read';
allRoles.push({
  roleId: PERMISSION_READ_ALL_ROLES,
  description: 'Xem tất cả Role',
});

export const PERMISSION_CREATE_PROFILE = 'profile.create';
allRoles.push({
  roleId: PERMISSION_CREATE_PROFILE,
  description: 'Tạo Profile',
});

export const PERMISSION_UPDATE_PROFILE = 'profile.update';
allRoles.push({
  roleId: PERMISSION_UPDATE_PROFILE,
  description: 'Cập nhật Profile',
});

export const PERMISSION_DELETE_PROFILE = 'profile.delete';
allRoles.push({
  roleId: PERMISSION_DELETE_PROFILE,
  description: 'Xóa Profile',
});

export const PERMISSION_READ_PROFILE = 'profile.read';
allRoles.push({
  roleId: PERMISSION_READ_PROFILE,
  description: 'Xem Profile',
});

export const PERMISSION_READ_ALL_PROFILES = 'allProfiles.read';
allRoles.push({
  roleId: PERMISSION_READ_ALL_PROFILES,
  description: 'Xem tất cả Profile',
});

export const PERMISSION_CREATE_LOCATION = 'location.create';
allRoles.push({
  roleId: PERMISSION_CREATE_LOCATION,
  description: 'Tạo Location',
});

export const PERMISSION_UPDATE_LOCATION = 'location.update';
allRoles.push({
  roleId: PERMISSION_UPDATE_LOCATION,
  description: 'Cập nhật Location',
});

export const PERMISSION_DELETE_LOCATION = 'location.delete';
allRoles.push({
  roleId: PERMISSION_DELETE_LOCATION,
  description: 'Xoá Location',
});

export const PERMISSION_READ_LOCATION = 'location.read';
allRoles.push({
  roleId: PERMISSION_READ_LOCATION,
  description: 'Xem Location',
});

export const PERMISSION_READ_ALL_LOCATIONS = 'allLocations.read';
allRoles.push({
  roleId: PERMISSION_READ_ALL_LOCATIONS,
  description: 'Xem tất cả Location',
});

export const PERMISSION_READ_ALL_LOCATIONS_OF_CUSTOMER =
  'allLocationsOfCustomer.read';
allRoles.push({
  roleId: PERMISSION_READ_ALL_LOCATIONS_OF_CUSTOMER,
  description: 'Xem tất cả Location của Owner',
});

export const roles = allRoles;
